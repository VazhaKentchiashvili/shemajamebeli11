package com.vazhasapp.shemajamebeli11.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli11.databinding.EmptyItemCardViewBinding
import com.vazhasapp.shemajamebeli11.databinding.PuzzleItemCardViewBinding
import com.vazhasapp.shemajamebeli11.model.EmptyCube
import com.vazhasapp.shemajamebeli11.model.PuzzleCube

typealias ItemSwipe = (position: Int) -> Unit

class PuzzleAdapter : RecyclerView.Adapter<PuzzleAdapter.PuzzleViewHolder>() {

    private val cubeList = mutableListOf<PuzzleCube>()
    lateinit var itemSwipe: ItemSwipe

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PuzzleViewHolder(
            PuzzleItemCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: PuzzleViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = cubeList.size

    inner class PuzzleViewHolder(private val binding: PuzzleItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var currentBlock: PuzzleCube

        fun bind() {
            currentBlock = cubeList[adapterPosition]
            binding.tvCubeNumber.text = currentBlock.cubeNumber
        }

        override fun onClick(v: View?) {
            itemSwipe(adapterPosition)
        }
    }


    fun setData(cubes: MutableList<PuzzleCube>) {
        cubeList.clear()
        cubeList.addAll(cubes)
        notifyDataSetChanged()
    }
}