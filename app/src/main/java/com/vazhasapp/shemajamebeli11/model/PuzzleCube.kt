package com.vazhasapp.shemajamebeli11.model

data class PuzzleCube(
    val position: Int,
    val cubeNumber: String,
)
