package com.vazhasapp.shemajamebeli11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.vazhasapp.shemajamebeli11.adapter.PuzzleAdapter
import com.vazhasapp.shemajamebeli11.databinding.ActivityMainBinding
import com.vazhasapp.shemajamebeli11.model.PuzzleCube
import kotlin.random.Random


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val myAdapter = PuzzleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        setupRecycler()
        gameLogic()

    }


    private fun setupRecycler() {

        binding.rvGame.apply {
            adapter = myAdapter
            layoutManager = GridLayoutManager(this@MainActivity, 3)
            setHasFixedSize(true)
        }

        myAdapter.setData(setDummyCubesData())

        myAdapter.itemSwipe = {



        }
    }

    private fun gameLogic() {

//        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(
//            0,
//            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
//        ) {
//            override fun onMove(
//                recyclerView: RecyclerView,
//                viewHolder: RecyclerView.ViewHolder,
//                target: RecyclerView.ViewHolder
//            ): Boolean {
//                return true
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
//                var cubePosition = viewHolder.adapterPosition
//                val currentCube = myAdapter.itemSwipe
//
//
//            }
//        }
//
//        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(binding.rvGame)
    }

    private fun setDummyCubesData(): MutableList<PuzzleCube> {

        val cubes = mutableListOf<PuzzleCube>()

        for (i in 1..8) {
            cubes.add(PuzzleCube(i, "$i"))
        }

        cubes.shuffle()
        return cubes
    }
}